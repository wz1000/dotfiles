#!/bin/bash
battery_level=`acpi -b | head -n 1 | grep -P -o '[0-9]+(?=%)'`
battery_charging=`acpi -b | head -n 1 | grep -P -o 'Charging'`

#Check if battery is correct
if [ $battery_level -eq 0 ]
then
    battery_level=`acpi -b | tail -n 1 | grep -P -o '[0-9]+(?=%)'`
    battery_charging=`acpi -b | tail -n 1 | grep -P -o 'Charging'`
fi

echo $battery_level
echo $battery_charging

if ([ $battery_level -le 25 ]) && [ -z "$battery_charging" ]
then
    env DBUS_SESSION_BUS_ADDRESS='unix:path=/run/user/1000/bus' notify-send -u critical "Battery low" "Battery level is ${battery_level}%!"
fi

