#!/bin/sh

aria2c --split=5 --max-connection-per-server=1 "$@"
if [ $? != "0" ]; then
  notify-send "Download failed" "$@"
else
   notify-send "Download succeeded" "$@"
fi
read
