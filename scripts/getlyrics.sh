#/bin/zsh
echo "<table align=center>"
echo "<tr>"
i=0
echo "<td align=center>"
while read l; do
    if (($i > 20)) 
    then
        echo "</td>"
        echo "<td align=center>"
        i=0
    fi
    echo $l
    ((i+=1))
done < /home/zubin/.lyrics/"$(mpc current)".txt
echo "</td>"
echo "</tr>"
echo "</table>"
