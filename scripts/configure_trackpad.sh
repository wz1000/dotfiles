#!/bin/bash
set -x
TRACKPAD=$(xinput list | grep -i 'CDA1 Touchpad' | grep -Po '(?<=id=)\d+')
##set natural scrolling
NSCROLL=$(xinput list-props $TRACKPAD | grep -Po '(?<=Natural Scrolling Enabled) \(\d+' | cut -c 3-)
xinput set-prop $TRACKPAD $NSCROLL 1

##change click method
CLICKMETH=$(xinput list-props $TRACKPAD | grep -Po '(?<=Tapping Enabled) \(\d+' | cut -c 3-)
xinput set-prop $TRACKPAD $CLICKMETH 1

##disable drag
DRAG=$(xinput list-props $TRACKPAD | grep -Po '(?<=Drag Enabled) \(\d+' | cut -c 3-)
xinput set-prop $TRACKPAD $DRAG 0

