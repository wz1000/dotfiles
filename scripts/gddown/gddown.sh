#!/bin/bash
set -x
URL="$1"
wget -k "$URL" -O page.html
TITLE=$(pup -f page.html 'span[itemprop="name"] text{}')
mkdir "$TITLE"
mv page.html "$TITLE"
cd "$TITLE"
M3U=$(grep -oe '"\S*\.m3u"' page.html | cut -c2- | rev | cut -c2- | rev | head -n 1)
wget "$M3U" -O list.m3u
sed -i -- 's/http:/https:/g' list.m3u
cat *.m3u | xargs -n 1 wget -c
