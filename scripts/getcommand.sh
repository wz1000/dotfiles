#!/bin/bash
echo cd "$(readlink -f /proc/$1/cwd);"
</proc/"$1"/cmdline xargs --no-run-if-empty -0 -n1 \
    bash -c 'printf "%q " "${1}"' /dev/null;
echo
