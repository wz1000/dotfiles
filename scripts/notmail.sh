#!/bin/bash
#List unread messages newer than last mailsync and count them
set -x

markread() {
  mv "$1" "$(dirname $1)/../cur/$(basename $1)S"
}

newmails=$(find ~/Mail/*/Inbox/new/ -type f -newer ~/Mail/.mailsynclastrun 2> /dev/null)
for m in $newmails; do
  content=$(mu view $m)
  subject=$(echo "$content" | grep -i "^Subject:" | head -n 1 | xml esc | sed 's#^Subject:\(.*\)#<b>Subject:</b><i>\1</i>#')
  from=$(echo "$content" | grep -i "^From:" | head -n 1 | xml esc | sed 's#^From:\(.*\)#<b>From:</b><i>\1</i>#')
  summ=$(echo "$content" | grep -v "^\w\+:" | head -n 3 | xml esc)
  (DISPLAY=:0 env DBUS_SESSION_BUS_ADDRESS='unix:path=/run/user/1000/bus' dunstify --icon=mail-message-new -t 30000 "GMail" "$from\n$subject\n$summ..." -A "markread,markread" | case $(cat) in "markread") markread "$m";; esac ) &
done
#Create a touch file that indicates the time of the last run of mailsync
touch ~/Mail/.mailsynclastrun

notmuch new
wait
