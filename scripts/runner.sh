#!/bin/zsh

ARGS=$@
com=""

BROWSER=firefox

if [[ $ARGS =~ "[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)" ]]; then
    com+="$BROWSER $ARGS"
fi

compl=$(~/scripts/capture.zsh $ARGS | sed '/^\s*$/d' | head -n 10)


while read -r top; do
    top=$(echo $top | awk '{print $1}')
    com+=$( echo $ARGS | sed "s/\S*$//")$top"\n"
done <<< $compl

echo $com
