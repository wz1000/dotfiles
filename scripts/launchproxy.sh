#!/bin/sh
set -x

ssh -i ~/.ssh/hetzner-irc -vTND 4711 $(cat ~/.ssh/irchost) &
pid=$!
xmonadctl hook merge
chromium --incognito --proxy-server="socks://localhost:4711"
kill $pid
