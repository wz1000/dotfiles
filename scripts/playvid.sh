#!/bin/bash
set -x
if [ -z "$1" ]; then
  VID=$(xsel -o -b)
  NID=$(dunstify -p -t 0 "Loading video..." "${TITLE-$VID}")
  TITLE=$(youtube-dl -e "$VID")
  SUC=$?
  NID=$(dunstify -p -t 0 -r $NID "Loading video..." "${TITLE-$VID}")
  if [ $SUC != "0" ]; then
    VID=$(playerctl -p plasma-browser-integration metadata xesam:url)
    TITLE=$(playerctl -p plasma-browser-integration metadata xesam:title)
    NID=$(dunstify -p -t 0 -r $NID "Loading video..." "${TITLE-$VID}")
  fi
else
  VID="$1"
  NID=$(dunstify -p -t 0 "Loading video..." "${TITLE-$VID}")
  TITLE=$(youtube-dl -e "$VID")
  [ $? == 0 ] || exit 1
  NID=$(dunstify -p -t 0 -r $NID "Loading video..." "${TITLE-$VID}")
fi


((pidof mpv && [ -S /tmp/mpvytdl ] && echo '{ "command": ["loadfile", "'"$VID"'","append-play"] }' | socat -u - /tmp/mpvytdl) && dunstify -r $NID "mpv" "added video to queue:\n${TITLE-$VID}") \
  || (mpv --profile=pseudo-gui --input-ipc-server=/tmp/mpvytdl --x11-name="mpvytdl" --slang=en --ytdl-format="best[height<=800]/worst" --term-playing-msg=loadedmpvytldvid --ytdl-raw-options=sub-lang=en,write-auto-sub= "$VID" \
     | while read line; do [ "$line" = "loadedmpvytldvid" ] && dunstify -C $NID && break; done) \
  || (dunstify -t 5000 -r $NID "Failed to load video")

#youtube-dl --format best --write-auto-sub --sub-lang en -o - "$VID" | optirun mpv --x11-name="mpvytdl" --no-terminal -
