#!/bin/sh

autossh -M 12344 -i ~/.ssh/hetzner-irc zubin@$(cat ~/.ssh/irchost) 'tail -n0 -F /tmp/weechat.notify' \
  | tee | while IFS=$'\t' read t c; do notify-send "$t" "$c"; done
