#!/bin/sh

MESSAGE=$(cat)

NEWALIAS=$(echo "${MESSAGE}" | grep ^"From: " | sed s/[\,\"\']//g | awk '{$1=""; if (NF == 3) {print "alias" $0;} else if (NF == 2) {print "alias" $0 $0;} else if (NF > 3) {print "alias", tolower($2)"-"tolower($(NF-1)) $0;}}')

if grep -Fxq "$NEWALIAS" ~/Mail/aliases; then
    :
else
    echo "$NEWALIAS" >> ~/Mail/aliases
fi

sed -i '/notifications@github.com/d' ~/Mail/aliases
sed -i '/noreply/d' ~/Mail/aliases

echo "${MESSAGE}"
