#!/bin/bash

buf=$'\n'

buf+="# "$2""$'\n'
buf+="On $(date)"$'\n'
buf+="$(html2text <(perl -p -e 's/\n/<br>/' <<< "$3"))"$'\n'

printf "$buf" >> ~/.cache/dunst_history
