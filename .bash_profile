#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

source /home/zubin/.config/broot/launcher/bash/br
if [ -e /home/zubin/.nix-profile/etc/profile.d/nix.sh ]; then . /home/zubin/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
