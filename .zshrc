# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

#source /etc/zsh/zshrc
#source /usr/share/doc/pkgfile/command-not-found.zsh
 
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
source /usr/share/fzf/completion.zsh

source ~/.zsh_plugins.sh
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

HISTSIZE=500000
SAVEHIST=500000
HISTFILE=~/.zsh_history

setopt appendhistory autocd
setopt incappendhistory
setopt sharehistory
setopt autopushd pushdminus pushdsilent pushdtohome
setopt NO_CLOBBER
setopt MULTIOS
setopt NO_HUP
setopt nohup
setopt HIST_VERIFY
bindkey -v

if [ -n "$DISPLAY" ]; then
    export BROWSER=firefox
else
    export BROWSER=w3m
fi


alias rd=". ranger"
alias ranger=". ranger"
alias ls="exa --icons --color=auto"
alias mdtree="find . -type f -name '*.md' | tree --fromfile"
alias info="info --vi-keys"
alias pacin="sudo pacman -S"
alias gtypist="gtypist -w -t -b -e 1"
alias vim="~/scripts/kak-project"
alias kak="~/scripts/kak-project"
alias icat="kitty +kitten icat"

export PAGER="less -Ri"
export NNN_OPENER="/home/zubin/scripts/nuke"
export NNN_OPTS="acx"
export NNN_PLUG='p:preview-tui;d:dragdrop'
n ()
{
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    # The behaviour is set to cd on quit (nnn checks if NNN_TMPFILE is set)
    # If NNN_TMPFILE is set to a custom path, it must be exported for nnn to
    # see. To cd on quit only on ^G, remove the "export" and make sure not to
    # use a custom path, i.e. set NNN_TMPFILE *exactly* as follows:
    #     NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}



source ~/.zsh_funcs


autoload -Uz compinit
if [[ -n ${ZDOTDIR}/.zcompdump(#qN.mh+24) ]]; then
compinit;
else
compinit -C;
fi;

bindkey -- "^[[A" history-substring-search-up
bindkey -- "^[[B" history-substring-search-down

mkcomp() {
  gencomp "$@" && compinit
}

# stty ixany
# APOLLO_THEME=apollo
# 
# zstyle ':apollo*:*:core*:modules:left' modules 'dir' 'git' 'newline'
# zstyle ':apollo*:*:core*:modules:right' modules 'background_jobs' 'command_execution_time' 'vi_mode' 'clock' 'newline' 'status'
# zstyle ':apollo:scrollback*:core:modules:left' modules ''
# zstyle ':apollo:scrollback*:core:modules:right' modules 'status' 'command_execution_time' 'dir' 'clock'
# zstyle ':apollo:scrollback*:core:prompt:end' text '> '
# zstyle ':apollo:scrollback*:core:prompt:end' fg_color 'teal'
# 
# zstyle ':apollo*:*:*:*:git:*' elements "local_branch" "action" " " "staged" "|" "unstaged" "|" "untracked" " ⇡" "ahead" "⇣" "behind"
# zstyle ':apollo:*:*:*:git:*:ahead' fg_color "blue"
# zstyle ':apollo:*:*:*:git:*:behind' fg_color "red"
# 
# zstyle ':apollo:*:*:*:vi_mode:insert:main' text "INS"
# zstyle ':apollo:*:*:*:vi_mode:insert:main' fg_color "green"
# zstyle ':apollo:*:*:*:vi_mode:visual:main' text "VIS"
# zstyle ':apollo:*:*:*:vi_mode:visual:main' fg_color "yellow"
# zstyle ':apollo:*:*:*:vi_mode:normal:main' text "NORM"
# zstyle ':apollo:*:*:*:vi_mode:normal:main' fg_color "red"
# zstyle ':apollo:*:*:*:vi_mode:replace:main' text "REP"
# zstyle ':apollo:*:*:*:vi_mode:replace:main' fg_color "purple"
# zstyle ':apollo*:*:*:left:core*:links:top' text ""
# zstyle ':apollo*:*:*:left:core*:links:bot' text ""
# 
# zstyle ':apollo:*:*:*:clock:*' elements "24hour" ":" "min" ":" "sec"
# zstyle ':apollo:*:*:*:clock:*' verbose "false"
# zstyle ':apollo:*:*:*:clock:*' live "false"
# zstyle ':apollo:*:*:*:clock:*' style "bold"
# 
zstyle ':notify:*' command-complete-timeout 2

# fzf completion

zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:descriptions' format '[%d]'

FZF_TAB_GROUP_COLORS=(
    $'\033[94m' $'\033[32m' $'\033[33m' $'\033[35m' $'\033[31m' $'\033[38;5;27m' $'\033[36m' \
    $'\033[38;5;100m' $'\033[38;5;98m' $'\033[91m' $'\033[38;5;80m' $'\033[92m' \
    $'\033[38;5;214m' $'\033[38;5;165m' $'\033[38;5;124m' $'\033[38;5;120m'
)

zstyle ':fzf-tab:*' group-colors $FZF_TAB_GROUP_COLORS

# local extract="
# # trim input(what you select)
# local in=\${\${\"\$(<{f})\"%\$'\0'*}#*\$'\0'}
# # get ctxt for current completion(some thing before or after the current word)
# local -A ctxt=(\"\${(@ps:\2:)CTXT}\")
# # real path
# local realpath=\${ctxt[IPREFIX]}\${ctxt[hpre]}\$in
# realpath=\${(Qe)~realpath}
# "

# zstyle ':fzf-tab:complete:kak:*' extra-opts --preview-window=right --preview=$extract'~/scripts/fdpreview $realpath'
# zstyle ':fzf-tab:complete:cd:*' extra-opts --preview-window=right --preview=$extract'exa -1 --color=always $realpath'
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'exa -1 --color=always $realpath'
zstyle ':fzf-tab:complete:kak:*' fzf-preview  '[[ $group == "[files]" ]] && ~/scripts/fdpreview $realpath'
zstyle ':fzf-tab:complete:vim:*' fzf-preview '~/scripts/fdpreview $realpath'
zstyle ':fzf-tab:complete:kak-project:*' fzf-preview '~/scripts/fdpreview $realpath'

zstyle ':fzf-tab:complete:systemctl-*:*' fzf-preview 'SYSTEMD_COLORS=1 systemctl status $word'

enable-fzf-tab

source /usr/share/fzf/key-bindings.zsh

autoload -z edit-command-line
zle -N edit-command-line
bindkey "^X^E" edit-command-line

function set-title(){
  echo -n "\033]2;$1\007"
}
function preexec() { set-title "$PWD: $1" }
function precmd() {
  _LEXIT=$?
  if [ -z $LAST_HIST ]; then
    set-title "shell in $PWD";
else
    set-title "$PWD: exited $LAST_HIST with $_LEXIT";
  fi
}
function zshaddhistory() {
  if echo $1 | grep -q "\w"; then
    LAST_HIST="$1"
  fi
}
# function chpwd() { if [ ${KITTY_WINDOW_ID} ]; then kitty @ set-window-title --temporary zshrc "$PWD"; fi }

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

#source /usr/share/stderred/stderred.sh

#source /home/zubin/.config/broot/launcher/bash/br

export FORGIT_LOG_FZF_OPTS='--reverse'
export FORGIT_COPY_CMD='xclip -selection clipboard'


if [ -e /home/zubin/.nix-profile/etc/profile.d/nix.sh ]; then . /home/zubin/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

