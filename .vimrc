syntax enable
filetype indent on
colorscheme tomorrow-night
set termguicolors


let &t_ut=''

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set showcmd
set relativenumber
set wildmenu
set lazyredraw
set showmatch
set ignorecase
set smartcase

set incsearch
set hlsearch
nnoremap <leader><space> :nohlsearch<CR>
nnoremap j gj
nnoremap k gk
let mapleader=","

autocmd FileType haskell setl sw=2 sts=2 ts=2 et 

cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!<CR>
