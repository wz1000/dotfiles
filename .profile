export PATH="${PATH}:$HOME/.local/bin:$HOME/.cabal/bin:$HOME/.ghcup/bin"
export PAGER=/bin/less
export EDITOR=kak-project
export TERMINAL=kitty
export XDG_TERMINAL=kitty
export VISUAL=kak-project
export SHELL=zsh
export BROWSER=firefox
if [[ ! $DISPLAY && $XDG_VTNR -eq 1  ]]; then
  exec startx ~/xmonad_start.sh
fi
