#! /bin/bash
xsetroot -cursor_name left_ptr &

# setxkbmap -option compose:ralt
setxkbmap -option keypad:pointerkeys

export QT_QPA_PLATFORMTHEME="qt5ct"

# Prevent wine from polluting file associations
export WINEDLLOVERRIDES="winemenubuilder.exe=d"

# turn off Display Power Management Service (DPMS)
xset -dpms &
xset -b &

# setxkbmap -option ctrl:nocaps

# turn off black Screensaver
xset s off &

# clipster -d > /dev/null &

~/scripts/configure_trackpad.sh
calcurse --daemon

#Delhi
#redshift -l 28.65:77.21 &
#Chennai
#redshift -l 13.08:80.27 &

userresources=$HOME/.Xresources
usermodmap=$HOME/.Xmodmap

if [ -f "$userresources" ]; then
  xrdb -merge "$userresources"
fi

# if [ -f "$usermodmap" ]; then
#   xmodmap "$usermodmap"
# fi
kmonad ~/builds/kmonad/gk61.kbd &
kmonad ~/builds/kmonad/msi.kbd &

greenclip daemon &

# screen locking
xss-lock ~/scripts/lock &

playerctld daemon &

exec xmonad 2>~/xmonad-errs.log
